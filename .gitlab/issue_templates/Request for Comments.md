<!--

Use this template when you have an idea and would like others to comment on it.

1. Give your issue a good title
1. Complete all four sections below
1. Set a due date
1. Let relevant people know you're looking for their input
1. On the due date, read the comments and decide what to do next (e.g. start a 'Request a change' issue)

See this blog post for the inspiration for this approach: https://candost.blog/how-to-stop-endless-discussions/

-->
# Request for comments

## Need

<!-- 
Describe the problem, what is missing or what could be better: "I am hungy!"
-->

## Approach

<!--
Describe your idea to solve the need: "Let's go to Bob's Pizza Place"
-->

## Benefit

<!--
Describe the benefits of this approach: "It's close, good quality and good value."
-->

## Competition / Alternatives

<!--
This part is very important! Describe alternatives: "We could go McDonald's - the price is similar, but it's twice as far away."
-->


/label ~request-for-comments
/assign me
/label ~"FY25 OKR::Not Applicable" 
/label ~"team:us-government"
/label ~"Readiness::Triage"
